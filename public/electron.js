const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const path = require("path");
const isDev = require("electron-is-dev");
let mainWindow;

global.pathApp = app.getPath('userData');

function createWindow() {
    mainWindow = new BrowserWindow({
        width: 1200,
        height: 680,
        minHeight:680,
        minWidth:900,
        icon: __dirname + '/icon.png',
        backgroundColor: '#e6ecf0',
        webPreferences: {
            webSecurity: false,
            nodeIntegration: true
        }
    });
    mainWindow.loadURL(
        isDev
            ? "http://localhost:3000"
            : `file://${path.join(__dirname, "../build/index.html")}`
    );

    mainWindow.on("closed", () => (mainWindow = null));
}
app.on("ready", createWindow);
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});
app.on("activate", () => {
    if (mainWindow === null) {
        createWindow();
    }
});
