import React, {Component} from 'react'
import bcrypt from 'bcryptjs'
import {Dialog, DialogTitle, DialogContent, DialogActions, DialogButton} from '@rmwc/dialog';
import {Usuario, Op} from '../../db'
import './estilo.css'


class Entrar extends Component {

    state = {
        usuario: '',
        password: '',
        isOpen: false,
        msj: ''
    };

    handleLogin = e => {
        const {usuario, password} = this.state;
        if(usuario === ''){
            this.setState({
                isOpen:true,
                msj:'Ingresa tu nombre de usuario'
            })
        } else if(password === ''){
            this.setState({
                isOpen:true,
                msj:'Ingresa tu contraseña'
            })
        } else if (usuario.length < 4){
            this.setState({
                isOpen:true,
                msj: 'El nombre de usuario debe tener minimo 4 caracteres'
            })
        } else if(password.length < 6){
            this.setState({
                isOpen:true,
                msj:'La contraseña debe tener minimo 6 caracteres'
            })
        } else {
            Usuario.findOne({
                where: {
                    [Op.or]:{
                        usuario: usuario,
                        correo: usuario
                    }
                }
            }).then(result => {
                console.log(result);
                if (result != null) {
                    console.log(bcrypt.compareSync(password, result.password))
                    if (bcrypt.compareSync(password, result.password)){
                        sessionStorage.setItem("usuario", JSON.stringify(result));
                        this.props.history.push('/principal')
                    } else {
                        this.setState({
                            isOpen:true,
                            msj:'Contraseña incorrecta'
                        })
                    }
                } else {
                    this.setState({
                        isOpen:true,
                        msj:'No existe el usuario'
                    })
                }
            }).catch(e => {
                console.log(e)
            })
        }
    };

    handleUsuario = e => {
        this.setState({usuario: e.target.value})
    };

    handlePassword = e => {
        this.setState({password: e.target.value})
    };

    handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.handleLogin()
        }
    };

    render() {
        const {isOpen, msj} = this.state;
        return (
            <div id="entrar" className="flex justify-center items-center px-md py-xl">

                <Dialog
                    open={isOpen}
                    onClose={()=>this.setState({isOpen:false})}
                >
                    <DialogContent>{msj}</DialogContent>
                    <DialogActions>
                        <DialogButton action="accept" isDefaultAction>Aceptar</DialogButton>
                    </DialogActions>
                </Dialog>

                <div className="logo">
                    <h2>Fitness Sport</h2>
                </div>

                <div className="form">
                    <p>Bienvenido</p>
                    <input onKeyPress={this.handleKeyPress} onChange={this.handleUsuario.bind(this)} value={this.state.usuario} placeholder="Usuario o correo" type="text"/>
                    <input onKeyPress={this.handleKeyPress} onChange={this.handlePassword.bind(this)} value={this.state.password} placeholder="Contraseña" type="password"/>
                    <button className="btn" onClick={this.handleLogin.bind(this)}>Iniciar sesión</button>
                    <button className="link">¿Olvidaste tu contraseña?</button>
                </div>

                <div className="version">
                    Version <span>0.1.2</span>
                </div>

                <p className="autor">JULIO CÉSAR CAICEDO SANTOS</p>
            </div>
        )
    }
}

export default Entrar
