import React, {Component} from 'react'
import {TopAppBar, TopAppBarNavigationIcon, TopAppBarRow, TopAppBarSection, TopAppBarFixedAdjust, TopAppBarTitle} from '@rmwc/top-app-bar'
import {Dialog, DialogTitle, DialogContent, DialogActions, DialogButton} from '@rmwc/dialog'
import {List, ListItem, ListDivider, ListItemPrimaryText} from '@rmwc/list'
import { Drawer, DrawerHeader, DrawerContent, DrawerTitle,DrawerSubtitle,} from '@rmwc/drawer';
import {TextField} from '@rmwc/textfield'
import moment from "moment";
import bcrypt from 'bcryptjs'
import ventaIcon from '../../img/cash-register.svg'
import clientIcon from '../../img/jogging.svg'
import './estilo.css'

import Caja from './components/caja'
import Clientes from './components/clientes'

import {Persona, Op, Usuario} from '../../db'


const defaultPersona = () => {
    return {
        documento: null,
        nombre: null,
        telefono:null,
        fechaInicio:null,
        fechaFin:null,
    }
};

class Principal extends Component{

    constructor(props){
        super(props);
        let user = JSON.parse(sessionStorage.getItem('usuario'))
        this.state = {
            vista: 'caja',
            persona: defaultPersona(),
            msj: '',
            buscar:'',
            openMenu:false,
            accion: -1,
            pass: '',
            pass1:'',
            pass2:'',
            usuario:'',
            user: user
        };
    }

    handleMenu = (menu) => {
        this.setState({vista:menu})
    };

    buscarPersona = term => {
        Persona.findOne({
            where:{
                [Op.or]:{
                    documento: term,
                    nombre:{
                        [Op.like]: '%' + term + '%'
                    }
                }
            }
        }).then(persona => {
            if(persona != null){
                this.setState({persona:JSON.parse(JSON.stringify(persona))})
            } else {
                this.setState({msj:'No se encontro ningun cliente'})
            }
        }).catch(e => {
            this.setState({msj: e.message})
        })
    };

    handleEnterBuscar = e => {
        if(e.key === 'Enter'){
            this.buscarPersona(this.state.buscar)
        }
    };

    handlePassword = () => {
        const {pass, pass1, user} = this.state;
        bcrypt.compare(pass, user.password,(err, res) => {
            if (err){
                console.log(err);
                this.setState({msj:'La contraseña actual no coincide',pass1:'',pass:'',pass2:''})
            } else {
                var salt = bcrypt.genSaltSync(10);
                var hash = bcrypt.hashSync(pass1, salt);
                Usuario.update({
                    password: hash
                },{
                    where: {
                        id: user.id
                    }
                }).then(result => {
                    let u = user;
                    u.password = hash;
                    this.setState({pass1:'',pass:'',pass2:'',accion:-1, user: u})
                }).catch(e => {
                    this.setState({msj:e.message})
                })
            }
        })
    };

    handleUsuario = () => {
        const {usuario, user} = this.state;
        Usuario.update({
            usuario: usuario
        },{
            where:{
                id:user.id
            }
        }).then(result => {
            let u = user;
            u.usuario = usuario;
            sessionStorage.setItem('usuario', JSON.stringify(u));
            this.setState({accion:-1,user:u, usuario:''})
        }).catch(e => {
            this.setState({msj:e.message})
        })
    };

    render() {
        const {vista, msj, persona, buscar, openMenu, accion, pass, pass1, pass2, usuario, user} = this.state;
        return (
            <div>

                <Dialog open={persona.documento != null} onClose={() => {this.setState({buscar:'',persona:defaultPersona()})}}>
                    <DialogTitle>Cliente</DialogTitle>
                    <DialogContent>
                        <div className="separador"></div>
                        <div className="row">
                            <img style={{objectFit:'cover', borderRadius:'100px'}} width="150px" height="150px" src={persona.foto}/>
                            <div className="ml-lg" style={{color:'black'}}>
                                <div className="row">
                                    <strong>{persona.nombre}</strong>
                                    <hr className="full-width no-borders no-margin"/>
                                    <span style={{marginTop:'-5px',fontSize:'12px'}}>Nombre</span>
                                </div>
                                <div className="row">
                                    <strong>{persona.documento}</strong>
                                    <hr className="full-width no-borders no-margin"/>
                                    <span style={{marginTop:'-5px',fontSize:'12px'}}>Documento</span>
                                </div>
                                <div className="row">
                                    <strong style={{color: (persona.fechaInicio == null || moment(persona.fechaFin).subtract(1, 'days') < moment().startOf('days') ? 'red': 'black')}}>{persona.fechaInicio == null ? 'Paga el dia' : (moment().startOf('days') <= moment(persona.fechaFin).subtract(1, 'days') ? 'Puede ingresar' : 'Fecha ya vencio')}</strong>
                                    <hr className="full-width no-borders no-margin"/>
                                    <span style={{marginTop:'-5px',fontSize:'12px'}}>Fecha {persona.fechaInicio == null ? null : moment(persona.fechaInicio).format('DD/MM/YYYY') + ' - ' + moment(persona.fechaFin).format('DD/MM/YYYY')}</span>
                                </div>
                            </div>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <DialogButton action="close" isDefaultAction>Aceptar</DialogButton>
                    </DialogActions>
                </Dialog>

                <Dialog open={accion == 0}>
                    <DialogTitle>Cambiar contraseña</DialogTitle>
                    <DialogContent>
                        La contraseña debe tener un minimo de 6 caracteres
                        <div className="separador"></div>
                        <div className="column mt-md">
                            <TextField type="password" value={pass} onChange={e => this.setState({pass:e.target.value})} outlined label="Actual contraseña"/>
                            <TextField type="password" value={pass1} onChange={e => this.setState({pass1:e.target.value})} outlined className="mt-md" label="Nueva contraseña"/>
                            <TextField type="password" value={pass2} onChange={e => this.setState({pass2:e.target.value})} outlined className="mt-md" label="Confirme nueva contraseña"/>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <DialogButton onClick={() => this.setState({pass:'',pass1:'',pass2:'',accion:-1})} action="close">Cancelar</DialogButton>
                        <DialogButton onClick={this.handlePassword} disabled={pass1 !== pass2 || pass1.length < 6 || pass === '' || pass === pass1} isDefaultAction>Cambiar contraseña</DialogButton>
                    </DialogActions>
                </Dialog>

                <Dialog open={accion == 1}>
                    <DialogTitle>Cambiar nombre de usuario</DialogTitle>
                    <DialogContent>
                        <div className="separador"></div>
                        <div className="column mt-md">
                            <TextField type="password" value={usuario} onChange={e => this.setState({usuario:e.target.value})} outlined label="Nuevo nombre de usuario"/>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <DialogButton onClick={() => this.setState({usuario:'',accion:-1})} action="close">Cancelar</DialogButton>
                        <DialogButton onClick={this.handleUsuario} disabled={usuario === '' || user.usuario === usuario} isDefaultAction>Cambiar usuario</DialogButton>
                    </DialogActions>
                </Dialog>

                <Dialog open={msj != ''} onClose={() => this.setState({msj:''})}>
                    <DialogContent>{msj}</DialogContent>
                    <DialogActions>
                        <DialogButton action="close" isDefaultAction>Aceptar</DialogButton>
                    </DialogActions>
                </Dialog>

                <Drawer modal open={openMenu} onClose={() => this.setState({openMenu: false})}>
                    <DrawerHeader>
                        <DrawerTitle>Fitness Sport</DrawerTitle>
                        <DrawerSubtitle>versión 0.1.0</DrawerSubtitle>
                    </DrawerHeader>
                    <DrawerContent>
                        <List>
                            <ListItem style={{cursor:'pointer'}} onClick={() => this.setState({accion:0,openMenu:false})}>Cambiar contraseña</ListItem>
                            <ListItem style={{cursor:'pointer'}} onClick={() => this.setState({accion:1,openMenu:false})}>Cambiar nombre de usuario</ListItem>
                            <ListDivider />
                            <ListItem style={{cursor:'pointer'}} onClick={() => {sessionStorage.removeItem("usuario");this.props.history.push('/')}}>Cerrar sesion</ListItem>
                        </List>
                        <p style={{color:'#717171',position:'absolute',bottom: 0,left: '16px'}}>Julio Cesar Caicedo Santos<br/>Ingeniero de sistemas</p>
                    </DrawerContent>
                </Drawer>

                <TopAppBar fixed={true} theme={['white', 'onWhite']}>
                    <TopAppBarRow>
                        <TopAppBarSection alignStart>
                            <TopAppBarNavigationIcon onClick={evt => this.setState({openMenu: !openMenu})} style={{color:'black'}} icon="menu" />
                            <TopAppBarTitle>Fitness Sport</TopAppBarTitle>
                            <input value={buscar} style={{maxWidth:'350px'}} onKeyPress={this.handleEnterBuscar} onChange={e => {this.setState({buscar:e.target.value})}} className="buscar ml-lg" placeholder="Identificar cliente"/>
                        </TopAppBarSection>
                        <TopAppBarSection alignEnd>
                            <ul className="menu">
                                <li className={vista == 'caja' ? 'active' : ''} onClick={() => this.handleMenu('caja')}>
                                    <img src={ventaIcon}/>
                                    Caja
                                </li>
                                <li className={vista == 'clientes' ? 'active' : ''} onClick={() => this.handleMenu('clientes')}>
                                    <img src={clientIcon}/>
                                    Clientes
                                </li>
                            </ul>
                        </TopAppBarSection>
                    </TopAppBarRow>
                </TopAppBar>
                <TopAppBarFixedAdjust>
                    <div className="contenido">
                        {
                            vista === 'caja' &&
                            <Caja/>
                        }
                        {
                            vista === 'clientes' &&
                            <Clientes/>
                        }
                    </div>
                </TopAppBarFixedAdjust>

            </div>
        )
    }

}

export default Principal