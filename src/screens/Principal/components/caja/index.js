import React,{Component} from 'react'
import { Chip } from '@rmwc/chip';
import { TextField } from '@rmwc/textfield';
import { Button } from '@rmwc/button';
import { IconButton } from '@rmwc/icon-button';
import { Dialog, DialogTitle, DialogContent, DialogActions, DialogButton} from '@rmwc/dialog';
import { List, SimpleListItem} from '@rmwc/list';
import Datepicker, {registerLocale} from 'react-datepicker'
import {es} from 'date-fns/locale'
import { DataTable, DataTableContent, DataTableHead, DataTableBody, DataTableHeadCell, DataTableRow, DataTableCell} from '@rmwc/data-table';
import { Fab } from '@rmwc/fab';
import Autocomplete from 'react-autocomplete'
import { Checkbox } from '@rmwc/checkbox';
import moment from 'moment'
import CurrencyFormat  from 'react-currency-format'
import './estilo.css'
import cajaImg from '../../../../img/caja.png'
import sequelize, {Caja as CajaDB, Pago, Persona} from '../../../../db'
import caja from "../../../../db/caja";

const turnoDefault = () => {
    return {
        id: null,
        valorBase: 0,
        total:0,
        efectivo:0,
        fecha: new Date(),
        cierre: null,
        cerrada: true,
    }
};

const personaDefault = () => {
    return {
        documento:null,
        nombre:null,
        telefono:null,
        fechaInicio:null,
        fechaFin:null,
    }
};

const ventaDefault = () => {
    return {
        id:null,
        fecha: new Date(),
        personaId: null,
        cajaId: null,
        valor:'',
        soloDia: true,
    }
};

class Caja extends Component {

    constructor(props){
        super(props);
        registerLocale('es', es)
    }

    state = {
        open:false,
        abrirTurno:false,
        cerrarTurno:false,
        turno:turnoDefault(),
        cajas:[],
        pagos: [],
        msj: '',
        vender:false,
        clientes:[],
        cliente: personaDefault(),
        buscar: '',
        venta: ventaDefault(),
        volver:false,
        eliminarPago:false,
        pago:{}
    };

    componentDidMount() {
        this.cargarCajas(true);
        this.cargarClientes();
    }

    cargarCajas = (isDefault) => {
        CajaDB.findAll({
            order:[['id', 'DESC']]
        }).then(result => {
            this.setState({cajas: JSON.parse(JSON.stringify(result))});
            if (result.length && isDefault === true){
                if(result[0].cerrada === false){
                    this.setState({
                        turno: JSON.parse(JSON.stringify(result[0])),
                        open: !result[0].cerrada
                    });
                    this.cargarPago(result[0].id)
                }
            }
        })
    };

    cargarPago = (cajaId) => {
        Pago.findAll({
            where:{
                cajaId: cajaId
            },
            include:[{model:Persona}],
            order: [['id', 'DESC']]
        }).then(result => {
            this.setState({pagos: JSON.parse(JSON.stringify(result))})
        })
    };

    cargarClientes = () => {
        Persona.findAll().then(result => {
            this.setState({clientes: JSON.parse(JSON.stringify(result))})
        })
    };

    formatearDia = (dia) => {
        let d = moment(dia).format('DD/MM/YYYY');
        let h = moment().format('DD/MM/YYYY');
        let oh = moment().subtract(1, 'days').format('DD/MM/YYYY')

        if (d == h) {
            return 'Hoy a las ' + moment(dia).format('hh:mm A')
        } else if (d == oh){
            return 'Ayer a las ' + moment(dia).format('hh:mm A')
        } else {
            return moment(dia).format('DD/MM/YYYY hh:mm A')
        }
    };

    handleOpenTurno = () => {
        const {turno} = this.state;
        CajaDB.create({
            fecha: moment().toDate(),
            valorBase: turno.valorBase
        }).then(result => {
            this.cargarCajas(true);
            this.setState({
                turno:JSON.parse(JSON.stringify(result)),
                open:true,
                abrirTurno:false
            });
            this.cargarPago(result.id)
        }).catch(e => {
            this.setState({msj: e.message})
        })
    };

    handleCerrarTurno = () => {
        const {turno} = this.state;
        CajaDB.update({
            cerrada: true,
            cierre: new Date(),
            efectivo: turno.efectivo,
        },{
            where:{
                id: turno.id
            }
        }).then(result => {
            this.cargarCajas(false);
            this.setState({turno: turnoDefault()});
            this.setState({
                cerrarTurno:false,
                open:false,
                pagos: []
            })
        })
    };

    onPressTurno = (turno) => {
        const {cajas} = this.state;
        this.setState({turno: turno,volver: true}, () => {
            this.cargarPago(turno.id);
            if(turno.id !== cajas[0].id && turno.cerrada === true){
                this.setState({open:false})
            }else if(turno.id === cajas[0].id && turno.cerrada === false) {
                this.setState({open:true,volver:false})
            }
        })
    };

    handleVolver = () => {
        const {cajas} = this.state;
        if(cajas[0].cerrada){
            this.setState({turno:turnoDefault(),pagos:[],open:false})
        } else {
            this.setState({turno:cajas[0]},() => {
                this.cargarPago(this.state.turno.id)
                if(this.state.turno.cerrada === false){
                    this.setState({open:true})
                }
            })
        }
        this.setState({volver:false})
    };

    handleVenta = () => {
        const {venta, cliente, turno} = this.state
        return sequelize.transaction(t => {
            return Pago.create(venta,{transaction:t}).then(v => {
                let temp = turno;
                temp.total = parseFloat(temp.total) + parseFloat(v.valor);
                return CajaDB.update(temp,{
                    where:{
                        id: turno.id
                    },
                    transaction: t
                }).then(c => {
                    return Persona.update(cliente,{
                        where:{
                            documento:cliente.documento
                        },
                        transaction:t
                    })
                })
            })
        }).then(result => {
            let t = JSON.parse(JSON.stringify(turno));
            t.total = parseFloat(t.total) + parseFloat(venta.valor);
            console.log(t);
            console.log(turno);
            this.setState({
                //turno:t,
                venta: ventaDefault(),
                cliente: personaDefault(),
                vender:false
            });
            this.cargarPago(t.id)
        }).catch(e => {
            this.setState({msj:e.message})
        })
    };

    handleEliminarPago = () => {
        const {pago,turno} = this.state
        return sequelize.transaction(tr => {
            return CajaDB.update({
                total: parseFloat(turno.total) - parseFloat(pago.valor)
            },{
                where:{
                    id: turno.id
                },
                transaction:tr
            }).then(result => {
                return Pago.destroy({
                    where:{
                        id: pago.id
                    },
                    transaction: tr
                }).then(result => {
                    return Persona.findOne({
                        where:{
                            documento: pago.personaId
                        },
                        transaction: tr
                    }).then(persona => {
                        let d1 = moment(persona.fechaInicio);
                        let d2 = moment(turno.fecha);
                        if(d1 >= d2){
                            return Persona.update({
                                fechaInicio:null,
                                fechaFin:null
                            },{
                                where:{
                                    documento: persona.documento
                                },
                                transaction:tr
                            })
                        } else {
                            return persona
                        }
                    })
                })
            })
        }).then(result => {
            console.log(result);
            this.setState({eliminarPago:false,pago:{}});
            this.cargarPago(turno.id);
            this.cargarCajas(true)
        }).catch(e => {
            this.setState({msj:e.message})
        })
    };

    render(){
        const {open, abrirTurno, turno, msj, cajas, cerrarTurno, pagos, vender, clientes, cliente, buscar, venta, volver, eliminarPago, pago} = this.state;
        return(
            <div className="flex flex-row">

                <Dialog open={abrirTurno}>
                    <DialogTitle>Abrir turno</DialogTitle>
                    <DialogContent>
                        <div className="text-center">El turno se encuentra <strong>cerrado</strong> en estos momentos, para empezar, cuéntanos cuál es tu base inicial</div>
                        <div className="flex justify-center mt-md">
                            <TextField
                                value={turno.valorBase}
                                onChange={e => {let t = turno; t.valorBase = e.target.value.replace(/[^0-9]/g,"");this.setState({turno:t})}}
                                label="Base incial"
                                outlined={true}
                                dense={true}
                                type="number"/>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <DialogButton onClick={() => this.setState({abrirTurno: false})} action="close">Cancelar</DialogButton>
                        <DialogButton onClick={this.handleOpenTurno.bind(this)} action="accept" isDefaultAction>Abrir</DialogButton>
                    </DialogActions>
                </Dialog>

                <Dialog open={cerrarTurno}>
                    <DialogTitle>Cerrar turno</DialogTitle>
                    <DialogContent>
                        <div className="flex justify-center pt-md">
                            <TextField
                                value={turno.efectivo}
                                onChange={e => {let t = turno; t.efectivo = e.target.value.replace(/[^0-9]/g,"");this.setState({turno:t})}}
                                label="Efectivo"
                                outlined={true}
                                dense={true}
                            />
                        </div>
                        <div className="row mt-lg">
                            <DataTable>
                                <DataTableContent>
                                    <DataTableHead>
                                        <DataTableRow>
                                            <DataTableHeadCell alignEnd={true}>Vendido</DataTableHeadCell>
                                            <DataTableHeadCell alignEnd={true}>Total efectivo</DataTableHeadCell>
                                            <DataTableHeadCell alignEnd={true}>Diferencia</DataTableHeadCell>
                                        </DataTableRow>
                                    </DataTableHead>
                                    <DataTableBody>
                                        <DataTableRow activated>
                                            <DataTableCell alignEnd={true}><CurrencyFormat value={turno.total} displayType={"text"} thousandSeparator={true} prefix={'$'}/></DataTableCell>
                                            <DataTableCell alignEnd={true}><CurrencyFormat value={turno.efectivo} displayType={"text"} thousandSeparator={true} prefix={'$'}/></DataTableCell>
                                            <DataTableCell style={{color: (turno.efectivo - turno.total) < turno.valorBase ? 'red': ''}} alignEnd={true}><CurrencyFormat value={turno.efectivo - (turno.valorBase + turno.total)} displayType={"text"} thousandSeparator={true} prefix={'$'}/></DataTableCell>
                                        </DataTableRow>
                                    </DataTableBody>
                                </DataTableContent>
                            </DataTable>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <DialogButton onClick={() => this.setState({cerrarTurno: false})}>Cancelar</DialogButton>
                        <DialogButton onClick={this.handleCerrarTurno.bind(this)} isDefaultAction={true}>Terminar cierre</DialogButton>
                    </DialogActions>
                </Dialog>

                <Dialog open={vender}>
                    <DialogTitle>Registrar venta</DialogTitle>
                    <DialogContent>
                        <div className="separador"/>
                        {
                            cliente.documento == null &&
                            <div className="relative-position" style={{maxHeigth:'200px'}}>
                                <Autocomplete
                                    getItemValue={(item) => item.nombre}
                                    items={clientes}
                                    renderInput={props => {return <input style={{width:'219px'}} placeholder="Buscar cliente" className="buscar" {...props}/>}}
                                    renderMenu={(items, value, style) => (
                                        <div style={{
                                            marginTop:'2px',
                                            overflow:'auto',
                                            border:'1px solid #eee',
                                            maxHeight:'200px',
                                            boxShadow:'0 2px 5px rgba(0,0,0,.5)',
                                            position: 'fixed',
                                            width: '219px',
                                            background: 'white',
                                            zIndex: 1,
                                            borderRadius: '5px'
                                        }}
                                             children={items}/>
                                    )}
                                    renderItem={(item, isHighlighted) => (
                                        <div className="py-xs px-md" style={{cursor:'pointer', background: isHighlighted ? 'lightgray' : 'white'}} key={item.documento} >
                                            <p className="text-bold no-margin" style={{color:'black'}}>{item.nombre}</p>
                                            <p className="no-margin">{item.documento}</p>
                                        </div>
                                    )}
                                    shouldItemRender={(item, value) => item.nombre.toLowerCase().indexOf(value.toLowerCase()) > -1 || item.documento.indexOf(value) > -1}
                                    value={buscar}
                                    onChange={e => this.setState({ buscar: e.target.value })}
                                    onSelect={(val, item) => {let t=venta;t.personaId = item.documento;this.setState({venta:t,cliente:JSON.parse(JSON.stringify(item))})}}
                                />
                            </div>
                        }
                        {
                            cliente.documento != null &&
                            <div className="row" style={{color:'black'}}>
                                <div className="column">
                                    <img style={{borderRadius:'100px', objectFit:'cover', margin:'auto'}} width="120px"  height="120px" src={cliente.foto} />
                                    <div className="row mt-md text-center">
                                        <div style={{display: 'flex',flexDirection: 'column',justifyContent: 'center',width:'100%'}}>
                                            <strong>{cliente.nombre}</strong>
                                            <hr style={{width:'100%',margin: 0,border: 'none'}}/>
                                            <span style={{marginTop:'-5px',fontSize:'14px'}}>Nombre</span>
                                        </div>
                                    </div>
                                    <div className="row text-center">
                                        <div style={{display: 'flex',flexDirection: 'column',justifyContent: 'center',width:'100%'}}>
                                            <strong>{cliente.documento}</strong>
                                            <hr style={{width:'100%',margin: 0,border: 'none'}}/>
                                            <span style={{marginTop:'-5px',fontSize:'14px'}}>Documento</span>
                                        </div>
                                    </div>
                                    <div className="row flex justify-center">
                                        <Button dense={true} outlined={true} onClick={() => {let t=venta;t.personaId=null;this.setState({venta:t,cliente:personaDefault()})}}>Cambiar</Button>
                                    </div>
                                </div>
                                <div className="column ml-md">
                                    <Checkbox
                                        label="Mensualidad"
                                        value={venta.soloDia}
                                        onChange={e => {
                                            let c = cliente
                                            if(e.target.checked === true){
                                                c.fechaInicio = new Date();
                                                c.fechaFin = new Date();
                                            } else {
                                                clientes.forEach(p => {
                                                    if (p.documento === cliente.documento){
                                                        c.fechaInicio = p.fechaInicio;
                                                        c.fechaFin = p.fechaFin;
                                                        return;
                                                    }
                                                })
                                            }
                                            let t=venta;
                                            t.soloDia=!e.target.checked;
                                            this.setState({venta:t,cliente:c})
                                        }}/>
                                    {
                                        venta.soloDia == false &&
                                        <div>
                                            <div className="row">
                                                <label>Fecha inicio</label>
                                                <Datepicker
                                                    className="input-datepicker"
                                                    locale="es"
                                                    popperClassName="full-width"
                                                    placeholderText="Fecha inicio"
                                                    showDisabledMonthNavigation
                                                    dateFormat="dd/MM/yyyy"
                                                    selected={cliente.fechaInicio||new Date()}
                                                    onChange={e => {let t = cliente;t.fechaInicio=e;this.setState({cliente:t})}}/>
                                            </div>
                                            <div className="row mt-xs">
                                                <label>Fecha fin</label>
                                                <Datepicker
                                                    className="input-datepicker"
                                                    locale="es"
                                                    placeholderText="Fecha fin"
                                                    showDisabledMonthNavigation
                                                    dateFormat="dd/MM/yyyy"
                                                    selected={cliente.fechaFin||new Date()}
                                                    onChange={e => {let t = cliente;t.fechaFin=e;this.setState({cliente:t})}}/>
                                            </div>
                                        </div>
                                    }
                                    <TextField value={venta.valor} onChange={e => {let t=venta;t.valor=e.target.value.replace(/[^0-9]/g,"");this.setState({venta:t})}} className="mt-md" outlined dense label="Valor"/>
                                </div>
                            </div>
                        }
                    </DialogContent>
                    <DialogActions>
                        <DialogButton onClick={() => this.setState({venta: ventaDefault(),vender:false,cliente:personaDefault()})} action="close">Cancelar</DialogButton>
                        <DialogButton onClick={this.handleVenta} disabled={venta.personaId == null || venta.cajaId == null || venta.valor == ''} isDefaultAction>Registrar venta</DialogButton>
                    </DialogActions>
                </Dialog>

                <Dialog open={eliminarPago}>
                    <DialogTitle>Eliminar pago</DialogTitle>
                    <DialogContent>Desea eliminar el pago?</DialogContent>
                    <DialogActions>
                        <DialogButton onClick={() => this.setState({eliminarPago:false,pago:{}})} action="close">No</DialogButton>
                        <DialogButton onClick={this.handleEliminarPago} isDefaultAction>Si</DialogButton>
                    </DialogActions>
                </Dialog>

                <Dialog open={msj !== ''} onClose={() => this.setState({msj:''})}>
                    <DialogContent>{msj}</DialogContent>
                    <DialogActions>
                        <DialogButton action="close" isDefaultAction>Aceptar</DialogButton>
                    </DialogActions>
                </Dialog>

                <div style={{width:'70%'}}>
                    <div className="box px-md pt-md">
                        <div className="column">
                            <div className="flex items-center full-width">
                                {
                                    volver === true &&
                                    <IconButton onClick={this.handleVolver}icon="chevron_left" label="Rate this!" />
                                }
                                <div style={{flex:1}}>
                                    <strong className="mr-xs" style={{fontSize:'18px'}}>Turno</strong>
                                    <Chip label={open ? 'Abierto' : 'Cerrado' } />
                                </div>
                                {
                                    volver === false &&
                                    <Button onClick={() => this.setState({abrirTurno:open ? false : true,cerrarTurno:open ? true : false})}
                                        className="float-right"
                                        raised={true}
                                        label={open ? 'Cerrar' : 'Abrir'}/>
                                }
                            </div>
                            <div className="row">
                                <DataTable className="no-borders full-width">
                                    <DataTableContent className="full-width">
                                        <DataTableHead>
                                            <DataTableRow>
                                                <DataTableHeadCell>Abierta</DataTableHeadCell>
                                                <DataTableHeadCell>Cerrada</DataTableHeadCell>
                                                <DataTableHeadCell alignEnd={true}>Base</DataTableHeadCell>
                                                <DataTableHeadCell alignEnd={true}>Venta</DataTableHeadCell>
                                                <DataTableHeadCell alignEnd={true}>Efectivo</DataTableHeadCell>
                                                <DataTableHeadCell alignEnd={true}>Diferencia</DataTableHeadCell>
                                            </DataTableRow>
                                        </DataTableHead>
                                        <DataTableBody>
                                            <DataTableRow>
                                                <DataTableCell>{this.formatearDia(turno.fecha)}</DataTableCell>
                                                <DataTableCell>{this.formatearDia(turno.cierre)}</DataTableCell>
                                                <DataTableHeadCell alignEnd={true}><CurrencyFormat value={turno.valorBase} displayType={"text"} thousandSeparator={true} prefix={'$'}/></DataTableHeadCell>
                                                <DataTableCell alignEnd={true}><CurrencyFormat value={turno.total} displayType={"text"} thousandSeparator={true} prefix={'$'}/></DataTableCell>
                                                <DataTableCell alignEnd={true}><CurrencyFormat value={turno.efectivo} displayType={"text"} thousandSeparator={true} prefix={'$'}/></DataTableCell>
                                                <DataTableCell style={{color: (turno.efectivo - turno.total) < turno.valorBase ? 'red': ''}} alignEnd={true}><CurrencyFormat value={turno.efectivo - (turno.valorBase + turno.total)} displayType={"text"} thousandSeparator={true} prefix={'$'}/></DataTableCell>
                                            </DataTableRow>
                                        </DataTableBody>
                                    </DataTableContent>
                                </DataTable>
                            </div>
                            <div className="separador"/>
                        </div>
                        <div className="column mt-lg pb-md">
                            <p className="text-center" style={{fontSize:'18px'}}><strong>Ventas</strong></p>
                            <div className="row">
                                <DataTable className="full-width">
                                    <DataTableContent className="full-width">
                                        <DataTableHead>
                                            <DataTableRow>
                                                <DataTableHeadCell>Pago N°</DataTableHeadCell>
                                                <DataTableHeadCell className="text-center">Hora</DataTableHeadCell>
                                                <DataTableHeadCell className="text-center">Documento</DataTableHeadCell>
                                                <DataTableHeadCell className="text-center">Cliente</DataTableHeadCell>
                                                <DataTableHeadCell alignEnd={true}>Valor</DataTableHeadCell>
                                                <DataTableHeadCell className="text-center">Mensualidad</DataTableHeadCell>
                                                <DataTableHeadCell alignEnd>Eliminar</DataTableHeadCell>
                                            </DataTableRow>
                                        </DataTableHead>
                                        <DataTableBody>
                                            {
                                                pagos.map((pago, i) => (
                                                    <DataTableRow key={i}>
                                                        <DataTableCell>{pago.id}</DataTableCell>
                                                        <DataTableCell className="text-center">{moment(pago.fecha).format('hh:mm A')}</DataTableCell>
                                                        <DataTableCell className="text-center">{pago.persona.documento}</DataTableCell>
                                                        <DataTableCell className="text-center">{pago.persona.nombre}</DataTableCell>
                                                        <DataTableCell alignEnd><CurrencyFormat value={pago.valor} displayType={"text"} thousandSeparator={true} prefix={'$'}/></DataTableCell>
                                                        <DataTableCell className="text-center">{pago.soloDia ? 'No' : 'Si'}</DataTableCell>
                                                        <DataTableCell alignEnd><IconButton onClick={() => this.setState({pago:pago, eliminarPago:true})} disabled={!open} icon="delete_forever"/> </DataTableCell>
                                                    </DataTableRow>
                                                ))
                                            }
                                        </DataTableBody>
                                    </DataTableContent>
                                </DataTable>
                                {
                                    pagos.length === 0 &&
                                    <p className="text-center full-width mt-md text-bold">No hay ventas registradas</p>
                                }
                            </div>
                        </div>
                    </div>
                </div>

                <div className="turnos">
                    {
                        cajas.length > 0 &&
                        <List className="box" twoLine={true}>
                            {
                                cajas.map((caja, i) => (
                                    <SimpleListItem
                                        key={i}
                                        selected={caja.id === turno.id}
                                        onClick={() => this.onPressTurno(caja)}
                                        text={"Cierre N°" + caja.id}
                                        secondaryText={caja.cierre != null ? this.formatearDia(caja.cierre) : 'Caja abierta'}
                                        meta={caja.cerrada ? 'Cerrada' : 'Abierta'}
                                    />
                                ))
                            }
                        </List>
                    }
                    {
                        cajas.length == 0 &&
                        <div className="box pa-md text-center">
                            <p><strong>Registro de cajas</strong></p>
                            <img width="260px" src={cajaImg}/>
                            <p>Aca se mostraran todos los registros de caja</p>
                        </div>
                    }
                </div>
                {
                    open &&
                    <Fab onClick={() => {let t = venta;t.cajaId=turno.id;this.setState({venta:t,vender:true},() => {this.handleVolver()})}} className="fixed" style={{bottom:'24px',right:'24px'}} icon="add" label="Vender"/>
                }
            </div>
        )
    }
}

export default Caja