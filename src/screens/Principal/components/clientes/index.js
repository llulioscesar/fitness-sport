import React, {Component} from 'react'
import { List, SimpleListItem } from '@rmwc/list';
import moment from 'moment'
import { TextField } from '@rmwc/textfield';
import {Avatar} from '@rmwc/avatar'
import { Fab } from '@rmwc/fab';
import {Button} from '@rmwc/button'
import {Icon} from '@rmwc/icon'
import CurrencyFormat  from 'react-currency-format'
import { DataTable, DataTableContent, DataTableHead, DataTableBody, DataTableHeadCell, DataTableRow, DataTableCell} from '@rmwc/data-table';
import Webcam from "react-webcam";
import { Dialog, DialogTitle, DialogContent, DialogActions, DialogButton} from '@rmwc/dialog';
import './estilo.css'
import cautionSVG from '../../../../img/caution.svg'
import clienteImg from '../../../../img/cliente.png'
import {Persona, Pago, Op} from '../../../../db'

const fotoDefault = () => {
    return 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAMbAqsDASIAAhEBAxEB/8QAHAABAAMBAQEBAQAAAAAAAAAAAAUGBwQDAgEI/8QARBABAAECAwMFDQUHAwUBAAAAAAIDBAEFEgYTIhEyQlJxFCEjMTNBUWJygZGhwVNjgrLRJDVDYZKisRZE4RUlNHPxVP/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD+qQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARV/nljZ46Z1d5U6lPixBKioXG1dbk5La3px9apLV/jkcU9pMynzasI+zCP1BfBn3+oc0/wD1/wBkP0esNpcyhzpU5e1D9AXwU2ltbWj5e2py9mWMf1SVvtRZVceWrCtT92rD5AsA5LW/tbrHwFeE5dXV3/g6wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcV7mVpZ4ftFeEZdXx4/DAHa8rm4pW1GVavLCNOPjxV292row4bOjOpLrVOGKtXt/c31XVc1dXVj0YgkM4z6vfSnToeBt/nLtQoAAAAAAAO+1zq+tPJ3E5R6tTiwcAC3WO1VOXDeUdPrU+LD4J60vra8hqtqsKn8vP8GZkJSjPVHhkDVhQLLaC+tuGc99Hq1O/80/ZbT2tfhuIToy+OALAPKjXpXENVCpCpH0xx5XqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+ZSjCOqXDHB+TnGMJSlLTHDx4qRtBnkr6c7e24bePS+0B153tHKWqjl09MfPV/RWJylKeqfFKXOkAAAAAAAAAAAAAAAAAPqlXq0J6qFWdOXWjPSnbLae7ocN1GFaP9MkAAvljtDY3PDOe5n1an6penONSOqnKMo4+eLLHrSr1qE9VCrOnL1Z6QaiKBQ2izCn/FhUj95FIUdrav8AFtIS9mfJ+oLeK1Hau26VvXj7PJJ6f6rsvsrr+jD9QWEV/wD1XY/ZXP8ARh+qQynNKOZRqSoRnHRycur+YJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB+P1UNqc4167G2lw/wAWX0Bz7R513ZPue2n+zx50vtP+EAAAAAAAAAAAAAAAAAAAAAAAAAAAAC77GU9OWTn9pVx+SkNE2eo7jJ7WPpjq+PfBJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5MzvKdjaTr1ej4o+nEEXtNmvcNDcUJ/tFTz9XBSHrd1p3NxKtUlqqT4sXkAAAAAAAAAAAAAAAAAAAAAAAAAAAADoy+3ld3tKhHpy0/h87TIRjCGEY+LBV9jbCUcJ3tWPO4af1xWoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABRdqsx7pve56cvA0OH2pedas8vO4Mtq1sPKc2HbizkAAAAAAAAAAAAAAAAAAAAAAAAAAAAB1Zbb915hb0OjOXF7LldWWXPc2YW9bo05R1dnnBpNOMaUIwhHTHDvYYPt+P0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFN21utVxStY82EdUu3FW3bnFbunMrqr1pS09mHewcQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL/szdd1ZTT1eUpeDl7vEl1O2Jr6Li4t+tHVH3f8A1cQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHhdVNxa1qn2cMZfDB7o3aGejJ7rH1NPx7wM7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABK7LVN3ndv62qP8Aa0FmeTS05taS+9j+ZpgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACL2m/cd32R/NglEXtN+47r8P5sAZ6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD1tJabulLqyj+ZqLLIc+LUwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEfn0deUXcfu5Y/BIOe9hvLG4p9anKPyBmIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADVmUw58WrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPx+gMuuKe6uKtP7OUo/B5JDaGnus5u49aWr499HgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9beOq4pR60otRZjlkdWY2ketVh+ZpwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKRtlS0ZrGp9pTw+XeQC2bcU+G1q+1HFUwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAd+Qx15xax+8jL4d9o6gbKR1Z1Q9XVL+1fwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQe2NPXk+r7OpGX0+qitHzunvMpu4/dyl8O+zgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFg2LjqzOrLq0pfmwXZUNh4ctW6l1Yxj8eVbwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAc97KNOxuJS5sacuX4MxT+1GZ1a93VtYz029PhlHrY4elAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAtWw0v/Nj7H1WxnGS5jLLbre6dVOXDOP8AJoNpXp3NCFajLVTnhqwxB7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAy6+lqvriXWqyl/c8nrfR03dxH72X5nkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAumxlTGWWVY9WrjyfDBS102Jj/wBrqy+9x/xgCxAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAzbOI7rNruP3svnJxJfaunu87q/eaZfJEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL5slDTk0JdacpfRQ2jZDS3WUWsfTDV8e+CQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABTtuaOm4t63WhKPw/+q0u+2NHCrlO8+xlhL3Y95SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfVKnKrVhTjzpS0x97UaUI0oQhHmxw5MFA2ao7/ADi35ObT8Jj7mhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5swo902Vejh04Yxw7WZc1qzPto7XubNqsejU8Jh7wRQAAAAAAAAAAAAAAAAAAAAAAAAAAAAALZsTbcFe6l0vB4f5xWpw5Nadx5db0ccOKMeWXbj43cAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAr+11jv7KNxDylDv4+x51gfM44SjjGXiBlYmdosoxy+rvKXFb1Jd71cfQhgAAAAAAAAAAAAAAAAAAAAAAAAAAErszZd2ZnDV5Oj4SX0R9rb1bu4jRoR1VJL/kmXRy203fOqS4py/mCSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABz31vC8tatvU5tSPIza4oVKFxOjV51OWmTUVN2zstNxSuo82pwz7cAVsAAAAAAAAAAAAAAAAAAAAAAAAHVllp3ZmFK36MpcXZ5wW3ZGwwtrHuiUfC1+L8HmT75hGMIYRj4sH0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjc+te7Mpr0+lhHVHtwST8BlQktoaG4zi4j0ZS3mHv76NAAAAAAAAAAAAAAAAAAAAAAAWfYm11Vbi6n0fBx/zirDRMhte48poU5c7GOqXbiCSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABVdtbTgoXUfFHwc/oqbTMxt43llVoS6ce92+ZmtWlKlVnTlwyjLTKIPkAAAAAAAAAAAAAAAAAAAAAHbklp3ZmdvR6OrVPswaQq2xNppo1bqfS8HDs861AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKXthYbq7heU/J1eGfaujkzC0p3lpVt6nNn5/RiDNB63VGdtcSo1OGpCWnF5AAAAAAAAAAAAAAAAAAAPqEZVZwpx4pS4YxfKc2StN/me8lzaEdXv8wLhl1tGzsqVCPQj8/O6gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABWtrcs39HuyhHlq0+fh1o/wDCnNVUTaXKu4bjeUo/s9Tm+rj6AQoAAAAAAAAAAAAAAAAAC7bH0MKeWSq486rPHH3Yd5SWjZDDRlFrh6Ycvx74JAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHFSzGhUvqtrq01odGXS7Htd21O6t5Ua8dVOXjUXaGUo59cSjPTKMo6ZR9nBM5JtFGpot8wlpqdGr0ZdoK7muXVctu93U4oy5k+tg42l5jZUr+1nRr83zY9XH0s+zOwq5fcbmv8Ahl0ZYA5QAAAAAAAAAAAAAAAGk5P+6rL/ANUf8M2aNkUtWT2mP3YJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGd7Sfvu79qP5cEaktpP33d+1H8uCNBNZPn1ex0U6vhrf0dKPYtMsLPPLLGOrCpT+cMWePW0uKtpV3lCrOnL1QdWb5VXy2rpqcVGXMn5nAtljtFbXlHufNaUI6uGUuhJxZrs/KMO6MslvreXFpjxS93pBAAAAAAAAAAAAAAAL/stLVklv6uqP92KgLzsfPVk+EerUlH6gnQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfE5xpwlKctMY+OUgfbyuK1KhT3lecacfTJXMy2nhDXTy+O8l15eL3YKxdXVe7q4TuKs6mOHWB7Z3cwuczuK1Dipyl3pfhcQAAAOqyv7mxlqtqs49aPR+DlATVW/ssw/wDOt9zcfb0frgj6tlKHFQnC5p9an9cPHg5QAJylLngAAAAAAAAAACybG3sadWra1JYR3nFDtwVshKUZ6o8MgasKVlu01ehpp3cd9T63TWqxvqF9S3lvPV6cPPgDrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUva6/nVusLOEuSnDnetiujM8zqbzMLup1qsvzA5QAAAAAAAAAAAAAAAAAAAAAAAHvZXdWzuI1qEtMo/3PABp9pXjdWtKtHm1I6nuhdlJasko+rKUfmmgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfFWpGnSlUlzY4crLJy1T1dZoO0lxhQye4l0px0R97PgAAAAAAAAAAAAAAAAAAAAAAAAAAXnY6erJ+Tq1JRTqpbD3HL3Rb+zUj/jFbQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQed59SsYypUPCXHyj2gids7zeXFKzh0OKfbirb6q1JVa06lWeqUuKUnyAAAAAAAAAAAAAAAAAAAAAAAAAADqyy7lY3tK4j0edHrYNHt61O5oQrUZaqcu/hiy5KZFm9TLaumWO8t5eOH1wBoQ8LS5pXdGNahLVCT3AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAByXuYW1lDVc1YR/l5/gDrcl9fW9jS13NTCPow88lZzHamc+Gxhu49eXj+CuVqlSvPeVZzqSl0pAnc12ir3Ouna+Bo9bpyQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6LG9r2NXeWs9Mul1Zdq3ZVtFbXWmnc+Brf2SUkBqr9Z3lub3WX8NOWqn1Jd/Bast2hs7vkjUluKnVl4viCaH4/QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB8ylGOGqXectXMbKl5S7ox/HgDsERV2gy2n47jV7MMXDW2rtY+St60va5I/qCyim1dq68vIW9OPtSxl+iPuM+zKv8A7jTH7vhBfa1WFKOqrOFOPplLSibzaSxoY6acpVpep4vio1apUqz1VZzqS60uJ8gnL3aS8ueGh+z0/V53xQs5SnPVKeqUudKT5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAd+X5reWPkKvg+pLiwWOy2qoVeG7pToy9MeKKmgNOt7qhcw1W9WFSPpjJ0MrhKVKeqM9MutFJ2m0OYUP4u8j1anF8/GDQRVLfa2H+5tZx9anLV8sUnb7QZbX/AI+7x6tSOkEwPGlcUa/ka1Op7EsMXsAAAAAAAAAAAAAAAAAAAAAAACHzHPrOy5Y699U6lP8AUEw8q1WFKOqrOFOPplLSpV7tJeV+Ghot4+r4/iha1SpVnqqznUl1pcQL7cbQZfQ/jbyXopx1Iy42tw/gWvvlL9FTATlXafMKnM3NP2Y/ryuCtm1/V513X/DPT/hxAPqdSpV4pTnL2nyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADopX93S8ld14/jk5wErS2hzKn/uNXtQi6qe1d5hz6VCXxw+qAAWqjtd9raf0z/4dVLauzlh4SjWj/Tj9VLAX+ltBltTxXGn2oYpC2uaFfyFWFT2Z6mYEJSjxR4ZA1YZ3aZ5mFtzbjeR6tTiT9jtTQqcN5S3MutHiiCyjyt61KvT3lCcakfTF6gAAAAAAAAAAOa9u6FjRxq3E8Ix+eLwzbMqOW2+8qcUpcyHWUPML2vfXG+rz1S6MejHsBIZrn1zfYzp0vA2/ojzpdqFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHva3Ve0q67arOnJb8n2ipXeMaV34Gtj4sejJSQGrCm7P59jQ0219PVR5sKkuj2riD9AAAAAAc97d07O1nXrcyPzdCh7UZj3Ze7mnPwNLhw9aXpBH5he1b67nWr9Lmx6uHocoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALVsnm3MsbmX/ql9FVISlCcJR4ZR4oyBqwj8kvf+oZdSrY8/mz7cEgAAAACJ2iv8bHLZ6fLVOCH6s/TW1d33TmcqcfJ0OHD2vOhQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWLYy63d3VtZc2rHVHtwXRl1pXlbXdKtHnU5Rk06lUjVpQqQ5so6sPeD7AAc97cRtbSrXl4qccZOhXds7nd5fChHnVpd/swBTZylOc5S4pS4pPkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAF52Ruu6MqjTlzqMtPu8yjJzY+63OZ7mXNqx0+/Dv4AvQACh7XXO/zaVOPNox0+/xrvXqxo0Z1Jc2EcZY+5mFxUlXqzrS51SUpS94PkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB9W9SVC4pVo86nKMo+58gNRt6sa1KFWHNnHCWHveqA2Ou9/lm6lzqEtPux8SfBVdsMx0w7ipS4pcVT6YKm97nHGtfXEquOMpb2XfxeAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJLIsx/6fewqS8jU4Z9npaDGUJRwlGUccMcOXDHV42WLFlN5cYZfSwwrTwww5cMMMMfF38Qf/9k='
}

const personaDefault = () => {
    return {
        nombre: '',
        documento: '',
        fechaInicio: null,
        fechaFin: null,
        telefono: '',
        peso:0,
        foto: fotoDefault(),
    }
};

const videoConstraints = {
    width: 1280,
    height: 720,
    facingMode: "user"
};

class Clientes extends Component{

    webcam = null;

    setRef = webcam => {
        this.webcam = webcam;
    };

    state = {
        editar: false,
        nueva: false,
        msj:'',
        eliminar:false,
        personas: [],
        pagos:[],
        persona: personaDefault(),
        startDate: new Date(),
        tempFoto: '',
        buscar:'',
        documento: 0,
        info:{
            total:0,
            plan:0,
            noplan:0,
            vencido:0
        }
    };

    componentDidMount() {
        this.cargarPersonas();
    }

    cargarPersonas = () => {
        Persona.findAll({
            order: [['nombre', 'ASC']]
        }).then(result => {
            let ps = 0;
            let nps = 0;
            let vs = 0;
            result.forEach(r => {
                if(r.fechaInicio != null){
                    let d1 = moment().startOf('days');
                    let d2 = moment(r.fechaFin).subtract(1, 'days');
                    if(d1 <= d2){
                        ps++;
                    } else{
                        vs++;
                    }
                } else {
                    nps++;
                }
            });
            this.setState({
                personas: JSON.parse(JSON.stringify(result)),
                info: {
                    total: result.length,
                    plan: ps,
                    noplan: nps,
                    vencido: vs
                }
            })
        })
    };

    cargarPagos = () => {
        const {persona} = this.state;
        Pago.findAll({
            where:{
                personaId: persona.documento
            },
            order:[['id', 'DESC']]
        }).then(result => {
            this.setState({pagos: JSON.parse(JSON.stringify(result))})
        })
    };

    capture = () => {
        const {persona, editar} = this.state;
        let t = persona;
        if (persona.foto === fotoDefault()){
            t.foto = this.webcam.getScreenshot();
            if(t.foto == null){
                t.foto = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB9AAAAfQCAYAAACaOMR5AAAABmJLR0QA/wD/'
            }
        } else {
            this.setState({tempFoto: t.foto});
            t.foto = fotoDefault()
        }
        this.setState({persona:t});
    };

    handlePersona = () => {
        const {persona, nueva, documento} = this.state;
        if (nueva){
            Persona.create(persona).then(() => {
                this.cargarPersonas();
                this.setState({
                    persona: personaDefault(),
                    nueva:false,
                    pagos:[],
                    buscar:''
                })
            }).catch(e => {
                this.setState({msj:e.message})
            })
        } else {
            Persona.update(persona,{
                where:{
                    documento: documento
                }
            }).then(() => {
                this.cargarPersonas();
                this.setState({
                    persona: personaDefault(),
                    editar:false,
                    tempFoto: '',
                    pagos:[],
                    documento: 0,
                    buscar:''
                })
            }).catch(e => {
                this.setState({msj:e.message})
            })
        }
    };

    formatearDia = (dia) => {
        let d = moment(dia).format('DD/MM/YYYY');
        let h = moment().format('DD/MM/YYYY');
        let oh = moment().subtract(1, 'days').format('DD/MM/YYYY')

        if (d == h) {
            return 'Hoy a las ' + moment(dia).format('hh:mm A')
        } else if (d == oh){
            return 'Ayer a las ' + moment(dia).format('hh:mm A')
        } else {
            return moment(dia).format('DD/MM/YYYY hh:mm A')
        }
    };

    handleEliminar = () => {
        const {persona} = this.state;
        Persona.destroy({
            where:{
                documento: persona.documento
            }
        }).then(() => {
            this.cargarPersonas();
            this.setState({persona:personaDefault(),eliminar:false})
        }).catch(e => {
            this.setState({msj:e.message})
        })
    };

    onPressPersona = persona => {
        this.setState({persona: persona},() => {
            this.setState({documento: persona.documento});
            this.cargarPagos();
        })
    };

    buscarPersona = term => {
        Persona.findAll({
            where:{
                [Op.or]:{
                    documento: {
                        [Op.like]: '%' + term + '%'
                    },
                    nombre:{
                        [Op.like]: '%' + term + '%'
                    }
                }
            }
        }).then(result => {
            this.setState({personas: JSON.parse(JSON.stringify(result))})
        })
    };

    render() {
        const {personas,persona, nueva, editar, eliminar, msj, tempFoto, pagos, buscar, info} = this.state;
        return(
            <div className="flex flex-row">

                <Dialog open={nueva||editar}>
                    <DialogTitle>{nueva ? 'Nueva': 'Editar'} persona</DialogTitle>
                    <DialogContent>
                        <div className="separador"></div>
                        <div className="row">
                            {
                                persona.foto === fotoDefault() &&
                                <Webcam
                                    id="foto"
                                    className="mt-md"
                                    audio={false}
                                    width={250}
                                    height={200}
                                    ref={this.setRef}
                                    screenshotFormat="image/jpeg"
                                    videoConstraints={videoConstraints}
                                />
                            }
                            {
                                persona.foto !== fotoDefault() &&
                                <img className="mt-md" width="250" height="200px" src={persona.foto}/>
                            }
                            <div className="column ml-lg">
                                <p><strong>Datos personales</strong></p>
                                <TextField value={persona.nombre} onChange={e => {let t = persona;t.nombre = e.target.value;this.setState({persona:t})}} outlined={true} dense={true} label="Nombre completo"/>
                                <TextField value={persona.documento} onChange={e => {let t = JSON.parse(JSON.stringify(persona));t.documento = e.target.value.replace(/[^0-9]/g,"");this.setState({persona:t})}} className="mt-md" outlined={true} dense={true} label="Documento"/>
                                <TextField value={persona.telefono} onChange={e => {let t = persona;t.telefono = e.target.value.replace(/[^0-9]/g,"");this.setState({persona:t})}} className="mt-md" outlined={true} dense={true} label="Telefono"/>
                                <TextField value={persona.peso} onChange={e => {let t = persona;t.peso = e.target.value.replace(/[^0-9]/g,"");this.setState({persona:t})}} className="mt-md" outlined={true} dense={true} label="Peso"/>
                            </div>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <DialogButton onClick={() => {this.setState({nueva:false,editar:false});if(editar && tempFoto !== ''){let t=persona;t.foto=tempFoto;this.setState({persona:t, tempFoto:''})}if(nueva){this.setState({persona:personaDefault()})}}}>Cancelar</DialogButton>
                        <DialogButton onClick={this.capture}>{persona.foto === fotoDefault() ? 'Capturar foto' : 'Elimina Foto'}</DialogButton>
                        <DialogButton onClick={this.handlePersona} isDefaultAction={true} disabled={persona.foto === fotoDefault() || persona.nombre === '' || persona.documento === '' || persona.telefono === ''}>Guardar</DialogButton>
                    </DialogActions>
                </Dialog>

                <Dialog open={eliminar}>
                    <DialogTitle>Eliminar cliente</DialogTitle>
                    <DialogContent>¿Desea eliminar este cliente?</DialogContent>
                    <DialogActions>
                        <DialogButton onClick={() => this.setState({eliminar:false})}>No</DialogButton>
                        <DialogButton onClick={this.handleEliminar} isDefaultAction>Si</DialogButton>
                    </DialogActions>
                </Dialog>

                <Dialog open={msj!==''} onClose={() => this.setState({msj:''})}>
                    <DialogContent>{msj}</DialogContent>
                    <DialogActions>
                        <DialogButton action="close" isDefaultAction>Aceptar</DialogButton>
                    </DialogActions>
                </Dialog>

                <div style={{width: '55%'}}>
                    <div className="box px-md">
                        {
                            persona.documento !== '' &&
                            <div>
                                <div className="row justify-center items-center pa-md">
                                    <div className="foto" style={{backgroundImage:'url('+persona.foto+')'}}/>
                                    <ul className="datos-basicos">
                                        <li style={{fontSize:'20px'}}><strong>{persona.nombre}</strong> </li>
                                        <li className="flex items-center"><Icon icon="picture_in_picture"/> {persona.documento}</li>
                                        <li className="flex items-center"><Icon icon="call"/> {persona.telefono}</li>
                                        <li className="flex items-center"><Icon icon="straighten"/>{persona.peso | 0}</li>
                                        <li className="flex items-center"><Icon icon="date_range"/> {persona.fechaInicio != null ? moment(persona.fechaInicio).format('DD/MM/YYYY') + ' - ' + moment(persona.fechaFin).format('DD/MM/YYYY') : 'Sin plan'}</li>
                                        <li>
                                            <Button onClick={() => this.setState({editar:true})}>Editar</Button>
                                            <Button onClick={() => this.setState({eliminar:true})}>Eliminar</Button>
                                        </li>
                                    </ul>
                                    <div className="separador"></div>
                                </div>
                                <div className="row pb-md">
                                    <DataTable className="full-width">
                                        <DataTableContent className="full-width">
                                            <DataTableHead>
                                                <DataTableRow>
                                                    <DataTableHeadCell className="text-center">Pago N°</DataTableHeadCell>
                                                    <DataTableHeadCell>Fecha</DataTableHeadCell>
                                                    <DataTableHeadCell alignEnd>Valor</DataTableHeadCell>
                                                    <DataTableHeadCell className="text-center">Mensualidad</DataTableHeadCell>
                                                </DataTableRow>
                                            </DataTableHead>
                                            <DataTableBody>
                                                {
                                                    pagos.map((pago, i) => (
                                                        <DataTableRow key={i}>
                                                            <DataTableCell className="text-center">{pago.id}</DataTableCell>
                                                            <DataTableCell>{this.formatearDia(pago.fecha)}</DataTableCell>
                                                            <DataTableCell alignEnd><CurrencyFormat value={pago.valor} displayType={"text"} thousandSeparator={true} prefix={'$'}/></DataTableCell>
                                                            <DataTableCell className="text-center">{pago.soloDia ? 'No' : 'Si'}</DataTableCell>
                                                        </DataTableRow>
                                                    ))
                                                }
                                            </DataTableBody>
                                        </DataTableContent>
                                    </DataTable>
                                    {
                                        pagos.length === 0 &&
                                        <p className="full-width text-center mt-xs">No ha realizado compras o pagos</p>
                                    }
                                </div>
                            </div>
                        }
                        {
                            persona.documento === '' &&
                            <div className="text-center text-bold pa-lg">
                                <img width="56px" className="text-center mb-lg" src={cautionSVG} />
                                <p>Selecciona un cliente para mostrar toda la informacion relacionada</p>
                            </div>
                        }
                    </div>
                </div>

                <div style={{width: '45%'}}>
                    {
                        personas.length > 0 &&
                        <div className="box">
                            <div className="relative-position pa-md">
                                <input value={buscar} onChange={e => {this.buscarPersona(e.target.value);this.setState({buscar:e.target.value})}} className="buscar" placeholder="Buscar"/>
                            </div>
                            <List twoLine={true}>
                                {
                                    personas.map((p, i) => (
                                        <SimpleListItem key={i}
                                            selected={p.documento == persona.documento}
                                            onClick={() => this.onPressPersona(p)}
                                            graphic={<Avatar src={p.foto} style={{width:'50px',height: '50px'}}/>}
                                            text={p.nombre}
                                            secondaryText={p.documento}
                                            meta={p.fechaInicio == null ? 'Sin plan': (moment().startOf('days') <= moment(p.fechaFin).subtract(1,'days') ? <span style={{color:'#4cd964'}}>{'hasta el ' + moment(p.fechaFin).format('DD/MM/YYYY')}</span> : <span style={{color:'#ff3b30'}}>{'Vencio el ' + moment(p.fechaFin).format('DD/MM/YYYY')}</span>)}
                                        />
                                    ))
                                }
                            </List>
                        </div>
                    }
                    {
                        personas.length === 0 &&
                        <div className="box pa-md text-center">
                            {
                                buscar !== '' &&
                                <input value={buscar} onChange={e => {this.buscarPersona(e.target.value);this.setState({buscar:e.target.value})}} className="buscar" placeholder="Buscar"/>
                            }
                            <p className="mt-md"><strong>Sin clientes</strong></p>
                            <img width="300px" src={clienteImg}/>
                            <p>{buscar == '' ? 'Aca se mostraran todos los clientes registrados' : 'No se encontraron cliente que coincidan con la busqueda'}</p>
                        </div>
                    }
                </div>
                <Fab onClick={() => {this.setState({nueva:true,editar:false,persona:personaDefault()})}} className="fixed" style={{zIndex: 2,bottom:'42px',right:'24px'}} icon="add" label="Registrar"/>
                <div className="fixed py-xs px-md" style={{background:'white',zIndex:1,bottom:0,left:0,right:0,fontSize:'12px',boxShadow:'0px 2px 15px 0px rgba(0,0,0,0.5)'}}>
                    <strong>clientes:</strong>{info.total}
                    <strong className="ml-md">con plan:</strong>{info.plan}
                    <strong className="ml-md">sin plan:</strong>{info.noplan}
                    <strong className="ml-md">plan vencido:</strong>{info.vencido}
                </div>

            </div>
        )
    }
}

export default Clientes