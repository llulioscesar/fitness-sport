import React from 'react'
import {HashRouter as Router, Route, Switch} from 'react-router-dom'
import 'material-components-web/dist/material-components-web.min.css'
import '@rmwc/avatar/avatar.css';
import '@rmwc/data-table/data-table.css';
import './css/icons.css';
import './css/app.css'
import './css/thema.css'
import "react-datepicker/dist/react-datepicker.css";

import Entrar from './screens/Entrar'
import Principal from './screens/Principal'

/*************************************************/
import db, {Usuario} from './db'
import Sequelize from "sequelize";

db.queryInterface.describeTable('persona').then(table => {
    if(!table['peso']){
        return db.queryInterface.addColumn('persona', 'peso',{
            type: Sequelize.INTEGER,
            allowNull:false,
            defaultValue:0
        });
    }else {
        return Promise.resolve(true)
    }
});

db.sync({sync: false})
    .then(() => {
        Usuario.findOne({
            where:{
                id:1
            }
        }).then(usuario => {
            if(usuario === null){
                Usuario.create({
                    nombre: 'admin',
                    usuario: 'admin',
                    correo: 'admin@email.com',
                    password: "$2a$10$WOe2mx4ovIXybGMX3KnHq.Fm9w2JTqDw8k2/0QF1G4spBnC4wG8um"
                })
            }
        })
    }).catch(e => {
        console.error(e);
        alert(e)
    });
/*************************************************/



export default () => (
    <Router>
        <Switch>
            <Route exact path="/" component={Entrar}/>
            <Route exact path="/principal" component={Principal}/>
        </Switch>
    </Router>

)