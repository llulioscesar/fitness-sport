import sequelize from './sequelize'
import {Sequelize} from 'sequelize'

export default sequelize.define('usuario',{
    id:{
        type:Sequelize.INTEGER,
        allowNull:false,
        autoIncrement:true,
        primaryKey:true
    },
    nombre:{
        type:Sequelize.TEXT,
        allowNull:false
    },
    usuario:{
        type:Sequelize.TEXT,
        allowNull:false,
        unique:true
    },
    password:{
        type:Sequelize.TEXT,
        allowNull:false
    },
    correo:{
        allowNull:false,
        type:Sequelize.TEXT,
        unique:true
    }
},{
    timestamps:false,
    freezeTableName:true,
    tableName:'usuario'
})