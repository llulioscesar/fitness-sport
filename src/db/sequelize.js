import path from 'path'
const {Sequelize} = window.require('sequelize');
const isDev =window.require('electron-is-dev');
const remote = window.require('electron').remote;

export const op = Sequelize.Op

let conf = {};
if (isDev){
    conf = {
        dialect: 'sqlite',
        storage: 'C:\\Users\\julio.caicedo\\Documents\\gym\\data.sqlite'
    }
    // C:\Users\julio.caicedo\Documents\gym
} else {
    let ruta = remote.getGlobal('pathApp');
    ruta = path.join(ruta, 'data.sqlite');
    conf = {
        dialect: 'sqlite',
        storage: ruta
    };
}

const sequelize = new Sequelize(null, null, null, conf);
export default sequelize;
