import sequelize from './sequelize'
import {Sequelize} from 'sequelize'

export default sequelize.define('caja',{
    id:{
        type:Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    valorBase:{
        type:Sequelize.DECIMAL(10,2),
        allowNull:false,
        defaultValue:0
    },
    efectivo:{
        type: Sequelize.DECIMAL(10,2),
        allowNull:false,
        defaultValue:0
    },
    total:{
        type: Sequelize.DECIMAL(10,2),
        allowNull:false,
        defaultValue:0
    },
    fecha:{
        type:Sequelize.DATE,
        allowNull:false
    },
    cierre:Sequelize.DATE,
    cerrada:{
        type:Sequelize.BOOLEAN,
        allowNull:false,
        defaultValue: false
    }
},{
    timestamps:false,
    tableName:'caja',
    freezeTableName:true
})