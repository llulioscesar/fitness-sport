import sequelize, {op} from './sequelize'
import persona from './persona'
import pago from './pago'
import caja from './caja'
import usuario from './usuario'


persona.hasMany(pago, {foreignKey:'personaId', onDelete:'cascade'});
pago.belongsTo(persona,{foreignKey:'personaId'});

caja.hasMany(pago, {foreignKey:'cajaId', onDelete:'cascade'});
pago.belongsTo(caja, {foreignKey:'cajaId'});


/**********************************/
export const Persona = persona;
export const Pago = pago;
export const Caja = caja;
export const Op = op;
export const Usuario = usuario;
/**********************************/
export default sequelize;