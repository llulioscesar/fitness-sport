import sequelize from './sequelize'
import {Sequelize} from 'sequelize'

export default sequelize.define('pago',{
    id:{
        type:Sequelize.INTEGER,
        autoIncrement:true,
        primaryKey:true
    },
    soloDia:{
        type:Sequelize.BOOLEAN,
        allowNull:false,
        defaultValue: false,
    },
    fecha:{
        type:Sequelize.DATE,
        allowNull:false
    },
    valor:{
        type:Sequelize.DECIMAL(10,2),
        allowNull:false,
    }
},{
    timestamps:false,
    freezeTableName:true,
    tableName: 'pago',
})